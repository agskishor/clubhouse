import 'package:clubhouse/utils/router.dart';
import 'package:clubhouse/services/authenticate.dart';
import 'package:clubhouse/utils/app_color.dart';
import 'package:clubhouse/widgets/style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:clubhouse/screens/home/home_screen.dart';
import 'package:clubhouse/screens/phone/phone_screen.dart';
//void main() => runApp(MyApp());

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = await SharedPreferences.getInstance();
  String userId = prefs.getString("id");
  if(userId == null)
    userId = "";
  if(userId.isNotEmpty){
    AuthService().getUserData();
  }
  runApp(MyApp(id: userId,));
}



/// This widget is the root of application
class MyApp extends StatelessWidget {
  final String id;
  const MyApp({Key key,this.id}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Clubhouse',
      debugShowCheckedModeBanner: false,
      onGenerateRoute: router,
      /*theme: ThemeData(
        scaffoldBackgroundColor: AppColor.LightBrown,
        appBarTheme: AppBarTheme(
          color: AppColor.LightBrown,
          elevation: 0.0,
          brightness: Brightness.light,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
      ),*/
      theme: Styles.darkTheme,
      themeMode: ThemeMode.light,
      home: id.isEmpty ? PhoneScreen() : HomeScreen(id: id)//AuthService().handleAuth(),
    );
  }
}
