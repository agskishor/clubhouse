/// Room model
/*{
"users_ids" : ["abc","xyz"],
"id" : "dfsf",
"speakerCount" : 1,
"status" : 0,
"title" : "dfsf",
"type" : 1,
"users" : [
{
"id" : "fsdfsd",
"name" : "sfsdf",
"role" : 1,
"profilePicture" : "sdfsdfsdfsdf",
"status" : 0
}
]
}*/
class Room {
  String id;
  int speakerCount;
  int status; //1 => running, 0 => finished
  String title;
  int type; //1 => Open, 2 => Social, 3 => Closed
  List<User> users;
  List<String> users_ids;

  Room(
      {this.id,
      this.speakerCount,
      this.status,
      this.title,
      this.type,
      this.users,
      this.users_ids});

  factory Room.fromJson(json) {
    return Room(
        id: json['id'],
        speakerCount: json['speakerCount'],
        status: json['status'],
        title: json['title'],
        type: json['type'],
        users: json['users'].map<User>((user) {
          return User(
            id: user['id'],
            name: user['name'],
            role: user['role'],
            profilePicture: user['profilePicture'],
            status: user['status'],
            speakRequest: user['speakRequest'],
          );
        }).toList(),
        users_ids: json['users_ids'].cast<String>());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['users_ids'] = this.users_ids;
    data['id'] = this.id;
    data['speakerCount'] = this.speakerCount;
    data['status'] = this.status;
    data['title'] = this.title;
    data['type'] = this.type;
    if (this.users != null) {
      data['users'] = this.users.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

/// User model
class User {
  final String id;
  final String name;
  final int role;//1 => audiance, 2 => Speaker, => Modarator
  final String profilePicture;
  int status; //1=> join, 0 => leave or ideal
  int speakRequest = 0; //0 => mute state,1 => requested, 2=> unmute state
  User({this.id, this.name, this.role, this.profilePicture, this.status,this.speakRequest});

  factory User.fromJson(json) {
    return User(
      id: json['id'],
      name: json['name'],
      role: json['role'],
      profilePicture: json['profilePicture'],
      status: json['status'],
      speakRequest: json['speakRequest'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['role'] = this.role;
    data['profilePicture'] = this.profilePicture;
    data['status'] = this.status;
    data['speakRequest'] = this.speakRequest;
    return data;
  }
}
