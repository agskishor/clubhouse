import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:clubhouse/core/data.dart';
import 'package:clubhouse/screens/followers/twitter_followings.dart';
import 'package:clubhouse/screens/phone/phone_screen.dart';
import 'package:clubhouse/utils/router.dart';
import 'package:clubhouse/screens/home/home_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthService {
  /// returns the initial screen depending on the authentication results
 /* handleAuth() {
    getUserData();
    return StreamBuilder(
      stream: FirebaseAuth.instance.onAuthStateChanged,
      builder: (BuildContext context, snapshot) {
        if (snapshot.hasData)  {
          return HomeScreen(id: 'jSUjdaaRA0Xh1Cmez0p5tX2KrDg1',);
        } else {
          return PhoneScreen();
        }
      },
    );
  }*/

  Future<String> checkFollowers() async {
    final prefs = await SharedPreferences.getInstance();
    List<dynamic> follower = [];
    var collection = Firestore.instance.collection('users');
    var myAccount = await collection.where('twitterId', isEqualTo: prefs.getString("id")).getDocuments();
    myAccount.documents.forEach((element) {
      follower = element.data['followers'];
    });
    if(follower.length == 0){
      return 'Twitter';
    } else {
      return 'Home';
    }
  }

  /// This method is used to logout the `FirebaseUser`
  signOut() async {
    FirebaseAuth.instance.signOut();
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  /// This method is used to login the user
  ///  `AuthCredential`(`_phoneAuthCredential`) is needed for the signIn method
  /// After the signIn method from `AuthResult` we can get `FirebaserUser`(`_firebaseUser`)
  signIn(BuildContext context, AuthCredential authCreds) async {
    AuthResult result =
        await FirebaseAuth.instance.signInWithCredential(authCreds);

    if (result.user != null) {
      addUser(result.user.uid, '');
      Navigator.of(context)
          .pushNamedAndRemoveUntil(Routers.home, (route) => false,arguments : result.user.uid);
    } else {
      print("Error");
    }
  }

  void addUser(String id, String twitterId) async{
    final prefs = await SharedPreferences.getInstance();
    prefs.setString("id", id);
    await getUserData();
    Firestore.instance.collection('users').document(id).setData({
      'name': prefs.getString("firstName") + ' ' + prefs.getString("lastName"),
      'userName': prefs.getString("firstName") + '_' + prefs.getString("lastName"),
      'profilePicture' : prefs.getString("profileImage"),
      'id' : id,
      'twitterId' : twitterId
    });
  }

  getUserData () async {
    final prefs = await SharedPreferences.getInstance();
    profileData = {
      'name': prefs.getString("firstName"),
      'username': prefs.getString("lastName"),
      'profilePicture': prefs.getString("profileImage"),
    };
  }
  /// get the `smsCode` from the user
  /// when used different phoneNumber other than the current (running) device
  /// we need to use OTP to get `phoneAuthCredential` which is inturn used to signIn/login
  signInWithOTP(BuildContext context, smsCode, verId) {
    AuthCredential authCreds = PhoneAuthProvider.getCredential(
        verificationId: verId, smsCode: smsCode);
    signIn(context, authCreds);
  }


}
