import 'package:flutter/material.dart';
import 'package:clubhouse/utils/app_color.dart';

class Styles {
  static get darkTheme => ThemeData(
      primaryColor: AppColor.primaryColor,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      brightness: Brightness.light,
      accentColor: Colors.black87,
      scaffoldBackgroundColor: AppColor.BackgroundColor,
      appBarTheme: AppBarTheme(
          color: AppColor.BackgroundColor,
          elevation: 0.0,
          iconTheme: IconThemeData(
            color: Colors.blueAccent,
          ),
        ),
  );
}