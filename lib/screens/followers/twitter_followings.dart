import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:clubhouse/screens/room/widgets/speaker.dart';
import 'package:clubhouse/utils/app_color.dart';
import 'package:clubhouse/utils/router.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:twitter_api/twitter_api.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class TwitterFollowing extends StatefulWidget {
  @override
  _TwitterFollowingState createState() => _TwitterFollowingState();
}

class _TwitterFollowingState extends State<TwitterFollowing> {

  twitterApi _twitterOauth;
  List<dynamic> followingList = [];
  List<dynamic> finalUserList = [];
  List<int> selectedUser = [];
  final collection = Firestore.instance.collection('users');
  final FirebaseAuth auth = FirebaseAuth.instance;

  @override
  void initState() {
      _twitterOauth =  twitterApi(
         consumerKey: 'A0yBrp0TlM4tigdf1YFB2m3k8',
         consumerSecret: 'YIFvqya8wabGh7dbYYPJqoFB2WUWVlqi006RNrx51cYpHtXBFG',
         token: '707806905348046849-XGSjxuu5HuLOxOWNcSEft39oWSlFa7Q',
         tokenSecret: 'Wsx6RHJKtqyUWNvziDZGVvS9yyNq8vnuIE8clQw9m73R5'
     );
     getFollowingUsers();

    super.initState();
  }

  Future<void> getFollowingUsers() async {
    final prefs = await SharedPreferences.getInstance();
    Future twitterRequest = _twitterOauth.getTwitterRequest(
      "GET",
      "friends/list.json",//"users/show.json",
      options: {
        "user_id": prefs.getString("twitterId")
      },
    );

    var res = await twitterRequest;
    if(res.statusCode == 200){
      var users = json.decode(res.body);
      Map<dynamic,dynamic> usersData = users;
      followingList = usersData['users'];
      getFirestoreData();
    } else {
      print('Error getting following list');
    }
  }

  Future<void> getFirestoreData() async {
    var firestoreData = await collection.getDocuments();
    final prefs = await SharedPreferences.getInstance();

    //common users of twitter followings and firestore collection
    for (var i = 0; i < followingList.length; i++) {
      bool isMatched;
      firestoreData.documents.forEach((element) {
        isMatched = element.data.containsValue(followingList[i]['id'].toString());
        if (isMatched) {
          setState(() {
            if (!(finalUserList.contains(followingList[i]))) {
              finalUserList.add(followingList[i]);
              selectedUser.add(finalUserList.length - 1);
            }
          });
        }
      });
    }
    // rest users from firestore collection
    firestoreData.documents.forEach((element) {
      bool isAdded = false;
      if(element.documentID != prefs.getString('id')) {
        //check if collection has not user who's already added
        for(var item in finalUserList){
          if(item['id'].toString() == element.data['twitterId']){
            isAdded = true;
          }
        }
        if(isAdded == false) {
          Map<String, String> firedata = {};
          firedata = {
            'id': element.data['id'],
            'name': element.data['name'],
            'profile_image_url': element.data['profilePicture'],
            'description': '',
            'fireStoreId': element.data['id'],
            'twitterId': element.data['twitterId']
          };
          setState(() {
            finalUserList.add(firedata);
          });
        }
      }
    });
    print(finalUserList);
    
    if(finalUserList.length == 0){
      final prefs = await SharedPreferences.getInstance();
      Navigator.of(context)
          .pushNamedAndRemoveUntil(Routers.home, (route) => false, arguments: prefs.getString('id'));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(),
      body: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.only(top: 0, bottom: 70),
            child: Column(
              children: [
                Text(
                  'Here are others we suggest!',
                  style: TextStyle(fontSize: 16),
                ),
                SizedBox(height: 4.0,),
                Text(
                  'You can edit this anytime.',
                  style: TextStyle(fontSize: 16),
                ),
                SizedBox(height: 25.0,),
                Expanded(
                    child: finalUserList.length > 0 ? Speaker(userList: finalUserList, isMultipleSelection: true,
                        selectedIndexList: selectedUser,
                        selectSpeaker: (value) {
                          selectedUser = value;
                        }) : Center(
                      child: CircularProgressIndicator(),
                    )
                )
              ],
            ),
          ),
          finalUserList.length > 0 ? Container(
            child: Column(
              children: [
                Expanded(child: Container(),),
                Column(
                  children: [
                    InkWell(
                      child: Container(
                        width: 170,
                        height: 40,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: AppColor.AccentBlue,
                          borderRadius: BorderRadius.all(
                            Radius.circular(25.0),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 16.0,),
                          child: Row(
                            crossAxisAlignment:
                            CrossAxisAlignment.center,
                            mainAxisAlignment:
                            MainAxisAlignment.center,
                            children: [
                              Text(
                                'Follow',
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white
                                ),
                              ),
                              SizedBox(
                                width: 4.0,
                              ),
                              Icon(Icons.arrow_forward, color: Colors.white),
                            ],
                          ),
                        ),
                      ),
                      onTap: () async {
                        final prefs = await SharedPreferences.getInstance();
                        List<String> followerList = [];
                        for (var i = 0; i < finalUserList.length; i++) {
                          if(selectedUser.contains(i)){
                            //check users who logged in by normal account
                            if (finalUserList[i]['fireStoreId'] != null) {
                              followerList.add(finalUserList[i]['fireStoreId']);
                            }
                            //users who logged in by twitter account
                            else {
                              var followerData = await collection.where('twitterId', isEqualTo: finalUserList[i]['id'].toString()).getDocuments();
                              followerData.documents.forEach((element) {
                                followerList.add(element.documentID);
                              });
                              //followerList.add(finalUserList[i]['id'].toString());
                            }
                          }
                        }
                        if(followerList.length > 0) {
                          var myAccount = await collection.where('twitterId', isEqualTo: prefs.getString("twitterId")).getDocuments();
                          myAccount.documents.forEach((element) {
                            Firestore.instance.collection('users').document(element.documentID).setData({
                              'followers': followerList,
                            }, merge: true);
                          });
                        }
                        Navigator.of(context)
                            .pushNamedAndRemoveUntil(Routers.home, (route) => false, arguments: prefs.getString('id'));
                      },
                    ),
                    SizedBox(height: 8.0,),
                    InkWell(
                      child: Text(
                        'or select individually',
                        style: TextStyle(fontSize: 15,
                            color: AppColor.AccentBlue),
                      ),
                      onTap: () {
                        setState(() {
                          selectedUser.clear();
                        });
                      },
                    ),
                    SizedBox(height: 8.0,),
                  ],
                ),
              ],
            ),
          ) : Container()
        ],
      ),
    );
  }
}
