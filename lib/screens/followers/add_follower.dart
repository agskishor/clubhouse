import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:clubhouse/screens/room/widgets/speaker.dart';
import 'package:clubhouse/utils/app_color.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddFollower extends StatefulWidget {
  @override
  _AddFollowerState createState() => _AddFollowerState();
}

class _AddFollowerState extends State<AddFollower> {

  List<dynamic> followingList = [];
  List<dynamic> finalUserList = [];
  List<int> selectedUser = [];
  List<dynamic> myFollowers = [];
  final collection = Firestore.instance.collection('users');
  final FirebaseAuth auth = FirebaseAuth.instance;
  bool isLoader = true;

  @override
  void initState() {
    getUserList();
    super.initState();
  }

  Future<void> getUserList() async {
    final prefs = await SharedPreferences.getInstance();
    var firestoreData = await collection.getDocuments();
    var myAccount = await collection.where('id', isEqualTo: prefs.getString("id")).getDocuments();
    myAccount.documents.forEach((element) {
      if(element.data['followers'] != null) {
        element.data['followers'].forEach((value) {
          myFollowers.add(value);
        });
      }
    });
      firestoreData.documents.forEach((element) {
        if(!(myFollowers.contains(element.documentID)) && (element.data['id'] != prefs.getString("id"))){
          Map<String, String> firedata = {};
          firedata = {
            'id': '',
            'name': element.data['name'],
            'profile_image_url': element.data['profilePicture'],
            'description': '',
            'fireStoreId': element.data['id']
          };
          followingList.add(firedata);
        }
      });
      setState(() {
        for (var i = 0; i < followingList.length; i++) {
          selectedUser.add(i);
        }
        finalUserList = followingList;
      });
      isLoader = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(),
      body: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.only(top: 0, bottom: 70),
            child: Column(
              children: [
                Text(
                  'Here are others we suggest!',
                  style: TextStyle(fontSize: 16),
                ),
                SizedBox(height: 4.0,),
                Text(
                  'You can edit this anytime.',
                  style: TextStyle(fontSize: 16),
                ),
                SizedBox(height: 25.0,),
                Expanded(
                    child: finalUserList.length > 0 ? Speaker(userList: finalUserList, isMultipleSelection: true,
                        selectedIndexList: selectedUser,
                        selectSpeaker: (value) {
                          selectedUser = value;
                        }) : Center(
                      child: isLoader ? CircularProgressIndicator() : Text('Data not found', style: TextStyle(fontSize: 20, color: Colors.white),),
                    )
                )
              ],
            ),
          ),
          finalUserList.length > 0 ? Container(
            child: Column(
              children: [
                Expanded(child: Container(),),
                Column(
                  children: [
                    InkWell(
                      child: Container(
                        width: 170,
                        height: 40,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: AppColor.AccentBlue,
                          borderRadius: BorderRadius.all(
                            Radius.circular(25.0),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(
                            left: 16.0,),
                          child: Row(
                            crossAxisAlignment:
                            CrossAxisAlignment.center,
                            mainAxisAlignment:
                            MainAxisAlignment.center,
                            children: [
                              Text(
                                'Follow',
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white
                                ),
                              ),
                              SizedBox(
                                width: 4.0,
                              ),
                              Icon(Icons.arrow_forward, color:Colors.white,),
                            ],
                          ),
                        ),
                      ),
                      onTap: () async {
                        List<String> followerList = [];
                        for (var i = 0; i < finalUserList.length; i++) {
                          if(selectedUser.contains(i)){
                            followerList.add(finalUserList[i]['fireStoreId']);
                          }
                        }
                        for (var i = 0; i < myFollowers.length; i++) {
                          followerList.add(myFollowers[i]);
                        }
                        if(followingList.length > 0) {
                          final prefs = await SharedPreferences.getInstance();
                          var myAccount = await collection.where('id', isEqualTo: prefs.getString("id")).getDocuments();
                          myAccount.documents.forEach((element) {
                            Firestore.instance.collection('users').document(element.documentID).setData({
                              'followers': followerList,
                            }, merge: true);
                          });
                        }
                        Navigator.pop(context);
                      },
                    ),
                    SizedBox(height: 8.0,),
                    InkWell(
                      child: Text(
                        'or select individually',
                        style: TextStyle(fontSize: 15,
                            color: AppColor.AccentBlue),
                      ),
                      onTap: () {
                        setState(() {
                          selectedUser.clear();
                        });
                      },
                    ),
                    SizedBox(height: 8.0,),
                  ],
                ),
              ],
            ),
          ) : Container()
        ],
      ),
    );
  }
}
