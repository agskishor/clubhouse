import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:clubhouse/models/models.dart';
import 'package:clubhouse/screens/home/HomeController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class RoomScreenNew extends StatefulWidget {
  final String roomId;
  final Room room;

  const RoomScreenNew({Key key, this.roomId, this.room}) : super(key: key);

  @override
  _RoomScreenNewState createState() => _RoomScreenNewState();
}

class _RoomScreenNewState extends State<RoomScreenNew> {
  final collection = Firestore.instance.collection('rooms');
  var room = Room();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    room = widget.room;
    getSnapRoom();
  }

  void getSnapRoom() {
    collection
        .where("id", isEqualTo: widget.roomId)
        .snapshots()
        .listen((result) {
      result.documents.forEach((result) {
        print("Document Id : " + result.documentID);
        if (result.documentID == widget.roomId) {
          setState(() {
            room = Room.fromJson(result);
          });
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(room.title),
    );
  }
}
