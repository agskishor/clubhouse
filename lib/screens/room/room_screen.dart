import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:clubhouse/models/models.dart';
import 'package:clubhouse/screens/room/widgets/user_profile.dart';
import 'package:clubhouse/screens/room/exit_bottom_sheet.dart';
import 'package:clubhouse/core/data.dart';
import 'package:clubhouse/utils/app_color.dart';
import 'package:clubhouse/core/settings.dart';
import 'package:clubhouse/widgets/rounded_button.dart';
import 'package:clubhouse/widgets/rounded_image.dart';
import 'package:flutter/material.dart';
import '../../core/settings.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:clubhouse/core/data.dart';

/// Detail screen of selected room
/// Initialize Agora SDK

class RoomScreen extends StatefulWidget {
  final Room room;
  final int role;
  final String id;

  const RoomScreen({Key key, this.room, this.role, this.id}) : super(key: key);

  @override
  _RoomScreenState createState() => _RoomScreenState();
}

class _RoomScreenState extends State<RoomScreen> {
  final collection = Firestore.instance.collection('rooms');
  var room = Room();

  final List _users = [];
  bool muted = false;
  RtcEngine _engine;
  var speakRequest = 2;
  var userId = "";

  @override
  void initState() {
    super.initState();
    // Initialize Agora SDK
    room = widget.room;
    print('role ' + widget.role.toString());
    getSnapRoom();
    initialize();
  }

  void getSnapRoom() async {
    final prefs = await SharedPreferences.getInstance();
    userId = prefs.getString("id");
    collection.where("id", isEqualTo: widget.id).snapshots().listen((result) {
      result.documents.forEach((result) {
        print("Document Id : " + result.documentID);
        if (result.documentID == widget.id) {
          setState(() {
            room = Room.fromJson(result);
          });
          room.users.forEach((element) {
            if (element.id == prefs.getString("id")) {
              print('speakRequest ' +
                  element.id +
                  ' ' +
                  element.speakRequest.toString() +
                  ' ' +
                  speakRequest.toString());
              if (speakRequest != element.speakRequest) {
                speakRequest = element.speakRequest;
                muteAudio();
              }
            }
          });
          if (room.status == 0) {
            Navigator.pop(context);
          }
        }
      });
    });
  }

  @override
  void dispose() {
    print('call dispose');
    //leave room
    leaveRoom();
    // Clear users
    _users.clear();
    // Destroy sdk
    _engine.leaveChannel();
    _engine.destroy();
    super.dispose();
  }

  void leaveRoom() {
    room.users.forEach((element) {
      if (element.id == userId) {
        print('leave room ' + element.id);
        element.status = 0;
      }
    });
    collection.document(room.id).updateData(room.toJson());
  }

  void exitRoom() async {
    room.status = 0;
    await collection.document(room.id).updateData(room.toJson());
    Navigator.pop(context);
  }

  /// Create Agora SDK instance and initialize
  Future<void> initialize() async {
    print('Toke in room' + agoraToken);
    await _initAgoraRtcEngine();
    _addAgoraEventHandlers();
    await _engine.joinChannel(agoraToken, channelName, null, 0);
    muteAudio();
  }

  Future<void> _initAgoraRtcEngine() async {
    _engine = await RtcEngine.create(APP_ID);
    await _engine.enableAudio();
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    await _engine.setClientRole(ClientRole.Broadcaster);
  }

  //mute
  Future<void> muteAudio() async {
    if(room.type == 1){
      await _engine.muteLocalAudioStream(false);
    }else {
      print("mute audio " + speakRequest.toString());
      if (speakRequest == 2)
        await _engine.muteLocalAudioStream(false);
      else
        await _engine.muteLocalAudioStream(true);
    }
  }

  /// Add Agora event handlers

  void _addAgoraEventHandlers() {
    _engine.setEventHandler(RtcEngineEventHandler(
      error: (code) {
        setState(() {
          print('onError: $code');
        });
      },
      joinChannelSuccess: (channel, uid, elapsed) {
        print('onJoinChannel: $channel, uid: $uid');
      },
      leaveChannel: (stats) {
        setState(() {
          print('onLeaveChannel');
          _users.clear();
        });
      },
      userJoined: (uid, elapsed) {
        print('userJoined: $uid');
        setState(() {
          _users.add(uid);
        });
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 150,
        automaticallyImplyLeading: false,
        title: Row(
          children: [
            IconButton(
              iconSize: 30,
              icon: Icon(
                Icons.keyboard_arrow_down,
                color: Colors.black87,
              ),
              onPressed: () => Navigator.pop(context),
            ),
            Text(
              'All rooms',
              style: TextStyle(color: Colors.black87, fontSize: 15),
            ),
            Spacer(),
            GestureDetector(
              onTap: () => Navigator.of(context)
                  .pushNamed('/profile', arguments: myProfile),
              child: Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                    color: Colors.orangeAccent,
                    borderRadius: BorderRadius.circular(40)),
                child: myProfile.profilePicture.length > 2
                    ? RoundedImage(
                        width: 40,
                        height: 40,
                        borderRadius: 20,
                        url: myProfile.profilePicture,
                      )
                    : Center(
                        child: Text(
                        myProfile.profilePicture,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      )),
              ),
            ),
          ],
        ),
      ),
      body: body(),
    );
  }

  Widget body() {
    print("role  in body " + room.users[0].role.toString());
    return Container(
      padding: const EdgeInsets.only(
        left: 20,
        right: 20,
        bottom: 20,
      ),
      decoration: BoxDecoration(
        color: AppColor.primaryColor,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(30),
          topLeft: Radius.circular(30),
        ),
      ),
      child: Stack(
        children: [
          SingleChildScrollView(
            padding: const EdgeInsets.only(bottom: 80, top: 20),
            child: Column(
              children: [
                title(room.title),
                SizedBox(height: 0),
                speakers(
                  room.users
                      .where((element) => element.role >= 2)
                      .where((element) => element.status == 1)
                      .toList(), //widget.room.users.sublist(0, widget.room.speakerCount > 2 ? 2 :widget.room.speakerCount),
                ),
                room.users
                            .where((element) => element.role == 1)
                            .where((element) => element.status == 1)
                            .toList()
                            .length >
                        0
                    ? others(
                        room.users
                            .where((element) => element.role == 1)
                            .where((element) => element.status == 1)
                            .toList(),
                      )
                    : Container(),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: bottom(context),
          ),
        ],
      ),
    );
  }

  Widget title(String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Flexible(
          child: Text(
            title,
            style: TextStyle(
                fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black87),
          ),
        ),
        // ignore: unrelated_type_equality_checks
        widget.role == 3
            ? Container(
                child: IconButton(
                  onPressed: () {
                    showModalBottomSheet(
                      context: context,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15),
                          topRight: Radius.circular(15),
                        ),
                      ),
                      builder: (context) {
                        return Wrap(
                          children: [
                            ExitBottomSheet(exitButton: exitCall),
                          ],
                        );
                      },
                    );
                  },
                  iconSize: 30,
                  icon: Icon(
                    Icons.more_horiz,
                    color: Colors.black87,
                  ),
                ),
              )
            : Container(),
      ],
    );
  }

  void exitCall() {
    Navigator.pop(context);
    print("exit pressed");
    exitRoom();
  }

  Widget speakers(List<User> users) {
    return GridView.builder(
      shrinkWrap: true,
      physics: ScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
      ),
      itemCount: users.length,
      itemBuilder: (gc, index) {
        return UserProfile(
          user: users[index],
          isModerator: index == 0,
          isMute: false,
          size: 60,
          onButtonTap: () {
            //print('hello button pressed');
          },
        );
      },
    );
  }

  Widget others(List<User> users) {
    print(users);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(16),
          child: Text(
            'Others in the room',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15,
              color: Colors.grey.withOpacity(0.6),
            ),
          ),
        ),
        GridView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
          ),
          itemCount: users.length,
          itemBuilder: (gc, index) {
            return UserProfile(
                user: users[index],
                size: 50,
                speakRequestStatus: users[index].speakRequest,
                onButtonTap: () {
                  if (widget.role == 3) {
                    if (users[index].speakRequest == 1)
                      speakRequestCall(2, users[index].id); //accept request
                    else if (users[index].speakRequest == 2)
                      speakRequestCall(0, users[index].id); // mute
                  }
                });
          },
        ),
      ],
    );
  }

  void speakRequestCall(int status, String speakUserId) {
    room.users.forEach((element) {
      print("speak req user Id : " + speakUserId + " " + element.id);
      if (element.id == speakUserId) {
        print('speakRequest ' + element.speakRequest.toString());
        if (element.speakRequest != status) {
          element.speakRequest = status;
        }
      }
    });

    collection.document(room.id).updateData(room.toJson());
  }

  Widget bottom(BuildContext context) {
    return Container(
      color: AppColor.primaryColor,
      child: Row(
        children: [
          RoundedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            color: AppColor.AccentGreen,
            child: Text(
              '✌️ Leave quietly',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Spacer(),
          widget.role == 1 ? RoundedButton(
            onPressed: () {
              print("call speak request");
              speakRequestCall(1, userId); //request for speak
            },
            color: AppColor.AccentGreen,
            isCircle: true,
            child: Icon(Icons.hail, size: 20, color: Colors.white),
          ) : Container(height: 40,width: 40,),
        ],
      ),
    );
  }
}
