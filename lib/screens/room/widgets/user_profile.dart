import 'package:clubhouse/models/models.dart';
import 'package:clubhouse/utils/app_color.dart';
import 'package:clubhouse/widgets/rounded_image.dart';
import 'package:flutter/material.dart';
import 'package:clubhouse/widgets/rounded_image.dart';

/// Shows the user's account icon in the room

class UserProfile extends StatelessWidget {
  final User user;
  final double size;
  final bool isMute;
  final bool isModerator;
  final int speakRequestStatus;
  final Function onButtonTap;

  const UserProfile(
      {Key key,
      this.user,
      this.size,
      this.isMute = true,
      this.speakRequestStatus = 0,
      this.isModerator = false,
      this.onButtonTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          children: [
            GestureDetector(
              onTap: onButtonTap,
              child: Container(
                width: size,
                height: size,
                decoration: BoxDecoration(
                    color: AppColor.LightBrown,
                    borderRadius: BorderRadius.circular(size/2)),
                child: user.profilePicture.length > 2
                    ? RoundedImage(
                        width: size-5,
                        height: size-5,
                        borderRadius: size/2,
                        url: user.profilePicture,
                      )
                    : Center(
                        child: Text(
                        user.profilePicture,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 12, color: Colors.black),
                      )),
              ),
            ),
            mute(isMute,speakRequestStatus),
          ],
        ),
        SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            moderator(isModerator),
            Flexible(
              child: Container(
                child: Text(
                  user.name.split(' ')[0],
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                  ),
                  textScaleFactor: 0.8,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  ///Return if user is moderator
  Widget moderator(bool isModerator) {
    return isModerator
        ? Container(
            margin: const EdgeInsets.only(right: 5),
            decoration: BoxDecoration(
              color: AppColor.AccentGreen,
              borderRadius: BorderRadius.circular(30),
            ),
            child: Icon(Icons.star, color: Colors.white, size: 12),
          )
        : Container();
  }

  ///Return if user is mute

  Widget mute(bool isMute,int speakStatus) {
    return Positioned(
      right: 0,
      bottom: 0,
      child: isMute
          ? Container(
              width: 20,
              height: 20,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(50),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    offset: Offset(0, 1),
                  )
                ],
              ),
              child: speakStatus == 0 ? Icon(
                Icons.mic_off,
                size: 15,
                color: Colors.black,
              ) : speakStatus == 1 ? Icon(
                Icons.hail,
                size: 15,
                color: Colors.black,
              ) : Icon(
                Icons.mic,
                size: 15,
                color: Colors.black,
              ),
            )
          : Container(),
    );
  }
}
