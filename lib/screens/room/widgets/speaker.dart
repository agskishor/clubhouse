import 'package:clubhouse/utils/app_color.dart';
import 'package:clubhouse/widgets/rounded_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef onTapCallBack = void Function(List);

class Speaker extends StatefulWidget {
  final onTapCallBack selectSpeaker;
  List<dynamic> userList;
  bool isMultipleSelection;
  List<int> selectedIndexList;
  int selectedIndex;

  Speaker(
      {Key key, @required this.userList, @required this.isMultipleSelection, @required this.selectedIndexList
      , @required this.selectedIndex, @required this.selectSpeaker})
      : super(key: key);

  @override
  _SpeakerState createState() {
    userList = this.userList;
    isMultipleSelection = this.isMultipleSelection;
    selectedIndexList = this.selectedIndexList;
    selectedIndex = this.selectedIndex;
    return _SpeakerState();
  }
}

class _SpeakerState extends State<Speaker> {
  @override
  void initState() {
   widget.selectedIndex = -1;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        physics: ClampingScrollPhysics(),
        scrollDirection: Axis.vertical,
        itemCount: widget.userList.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.only(bottom: 16.0, left: 16.0, right: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Flexible(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      (widget.userList[index]['profile_image_url'] ?? "").length == 2 ? Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: AppColor.primaryColor,
                          ),
                          child: Center(
                              child: Text(widget.userList[index]['profile_image_url'],
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 15),
                              )
                          )
                      ) : Container(
                        padding: const EdgeInsets.only(bottom: 5),
                        child: RoundedImage(
                          width: 50,
                          height: 50,
                          borderRadius: 20,
                          url: widget.userList[index]['profile_image_url'] ?? "",
                        ),
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Flexible(
                        child: Column(
                          crossAxisAlignment:
                          CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.userList[index]['name'],
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                IconButton(
                  icon: Icon(widget.isMultipleSelection ? ((widget.selectedIndexList.contains(index)) ?
                  Icons.check_circle : Icons.radio_button_unchecked) : ((widget.selectedIndex == index) ?
                  Icons.check_circle : Icons.radio_button_unchecked),
                    size: 30.0,
                    color: AppColor.AccentBlue,
                  ),
                  onPressed: () {
                    setState(() {
                      if(widget.isMultipleSelection) {
                        if(widget.selectedIndexList.contains(index)){
                          widget.selectedIndexList.remove(index);
                        } else {
                          widget.selectedIndexList.add(index);
                        }
                        widget.selectSpeaker(widget.selectedIndexList);
                      } else {
                        if(widget.selectedIndex == index){
                          widget.selectedIndex = -1;
                        } else {
                          widget.selectedIndex = index;
                        }
                        widget.selectSpeaker([widget.selectedIndex]);
                      }
                    });
                  },
                )
              ],
            ),
          );
        });
  }
}
