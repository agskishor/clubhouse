import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:clubhouse/screens/room/widgets/speaker.dart';
import 'package:clubhouse/utils/app_color.dart';
import 'package:clubhouse/widgets/rounded_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SpeakerScreen extends StatefulWidget {
  bool isTypeSocial;

  SpeakerScreen(
      {Key key, @required this.isTypeSocial})
      : super(key: key);

  @override
  _SpeakerScreenState createState() {
    isTypeSocial = this.isTypeSocial;
    return _SpeakerScreenState();
  }
}

class _SpeakerScreenState extends State<SpeakerScreen> {

  List<dynamic> SpeakerScreen = [];
  List<dynamic> finalUserList = [];
  final collection = Firestore.instance.collection('users');
  final FirebaseAuth auth = FirebaseAuth.instance;
  bool isLoader = true;
  int selectedSpeaker = -1;
  List<int> selectedSpeakerList = [];

  @override
  void initState() {
    getUserList();
    super.initState();
  }

  Future<void> getUserList() async {
    final prefs = await SharedPreferences.getInstance();
    var firestoreData = await collection.getDocuments();
    firestoreData.documents.forEach((element) {
      if(element.data['id'] != prefs.getString("id")){
        Map<String, String> firedata = {};
        firedata = {
          'id': '',
          'name': element.data['name'],
          'profile_image_url': element.data['profilePicture'],
          'description': '',
          'fireStoreId': element.data['id']
        };
        SpeakerScreen.add(firedata);
      }
    });
    setState(() {
      finalUserList = SpeakerScreen;
    });
    isLoader = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(),
      body: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.only(top: 30, bottom: 70),
            child: Column(
              children: [
                Text(
                  widget.isTypeSocial ? 'Choose Speaker' : 'Select members',
                  style: TextStyle(fontSize: 22),
                ),
                SizedBox(height: 25.0,),
                Expanded(
                    child: finalUserList.length > 0 ? Speaker(userList: finalUserList, isMultipleSelection: widget.isTypeSocial ? false : true,
                        selectedIndexList: [],
                        selectSpeaker: (value) {
                          if(widget.isTypeSocial){
                            selectedSpeaker = value[0];
                          } else {
                            selectedSpeakerList = value;
                          }
                        }) : Center(
                      child: isLoader ? CircularProgressIndicator() : Text('Data not found', style: TextStyle(fontSize: 20, color: Colors.white),),
                    )
                )
              ],
            ),
          ),
          finalUserList.length > 0 ? Container(
            child: Column(
              children: [
                Expanded(child: Container(),),
                Column(
                  children: [
                    InkWell(
                      child: Container(
                        width: 170,
                        height: 50,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: AppColor.AccentBlue,
                          borderRadius: BorderRadius.all(
                            Radius.circular(25.0),
                          ),
                        ),
                        child: Text(
                          widget.isTypeSocial ? 'Select' : 'Add',
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: Colors.white
                          ),
                        ),
                      ),
                      onTap: ()  {
                        if(widget.isTypeSocial) {
                          Navigator.pop(context, [true, finalUserList[selectedSpeaker]]);
                        } else {
                          List<dynamic> selectedList = [];
                          for (var i = 0; i < selectedSpeakerList.length; i++) {
                            selectedList.add(finalUserList[selectedSpeakerList[i]]);
                          }
                          Navigator.pop(context, [true, selectedList]);
                        }
                      },
                    ),
                    SizedBox(height: 20.0,),
                  ],
                ),
              ],
            ),
          ) : Container()
        ],
      ),
    );
  }
}
