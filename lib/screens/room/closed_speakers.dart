import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:clubhouse/utils/app_color.dart';
import 'package:clubhouse/widgets/rounded_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ClosedSpeakers extends StatefulWidget {
  @override
  _ClosedSpeakersState createState() => _ClosedSpeakersState();
}

class _ClosedSpeakersState extends State<ClosedSpeakers> {

  List<dynamic> ClosedSpeakers = [];
  List<dynamic> finalUserList = [];
  final collection = Firestore.instance.collection('users');
  final FirebaseAuth auth = FirebaseAuth.instance;
  bool isLoader = true;
  List<int> selectedSpeaker = [];

  @override
  void initState() {
    getUserList();
    super.initState();
  }

  Future<void> getUserList() async {
    final prefs = await SharedPreferences.getInstance();
    var firestoreData = await collection.getDocuments();
    firestoreData.documents.forEach((element) {
      if(element.data['id'] != prefs.getString("id")){
        Map<String, String> firedata = {};
        firedata = {
          'id': '',
          'name': element.data['name'],
          'profile_image_url': element.data['profilePicture'],
          'description': '',
          'fireStoreId': element.data['id']
        };
        ClosedSpeakers.add(firedata);
      }
    });
    setState(() {
      finalUserList = ClosedSpeakers;
    });
    isLoader = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(),
      body: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.only(top: 30, bottom: 70),
            child: Column(
              children: [
                Text(
                  'Choose Speaker',
                  style: TextStyle(fontSize: 22),
                ),
                SizedBox(height: 25.0,),
                Expanded(
                    child: finalUserList.length > 0 ? ListView.builder(
                        physics: ClampingScrollPhysics(),
                        scrollDirection: Axis.vertical,
                        itemCount: finalUserList.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.only(bottom: 16.0, left: 16.0, right: 16.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Flexible(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      (finalUserList[index]['profile_image_url'] ?? "").length == 2 ? Container(
                                          width: 50,
                                          height: 50,

                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(20),
                                            color: Colors.white,
                                          ),
                                          child: Center(
                                              child: Text(finalUserList[index]['profile_image_url'],
                                                style: TextStyle(
                                                    color: AppColor.BackgroundColor,
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 15),
                                              )
                                          )
                                      ) : Container(
                                        padding: const EdgeInsets.only(bottom: 5),
                                        child: RoundedImage(
                                          width: 50,
                                          height: 50,
                                          borderRadius: 20,
                                          url: finalUserList[index]['profile_image_url'] ?? "",
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      Flexible(
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              finalUserList[index]['name'],
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                IconButton(
                                  icon: Icon(selectedSpeaker.contains(index) ? Icons.check_circle : Icons.radio_button_unchecked,
                                    size: 30.0,
                                    color: AppColor.AccentBlue,
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      if(selectedSpeaker.contains(index)){
                                        selectedSpeaker.remove(index);
                                      } else {
                                        selectedSpeaker.add(index);
                                      }
                                    });
                                  },
                                )
                              ],
                            ),
                          );
                        }) : Center(
                      child: isLoader ? CircularProgressIndicator() : Text('Data not found', style: TextStyle(fontSize: 20, color: Colors.white),),
                    )
                )
              ],
            ),
          ),
          finalUserList.length > 0 ? Container(
            child: Column(
              children: [
                Expanded(child: Container(),),
                Column(
                  children: [
                    InkWell(
                      child: Container(
                        width: 170,
                        height: 50,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: AppColor.AccentBlue,
                          borderRadius: BorderRadius.all(
                            Radius.circular(25.0),
                          ),
                        ),
                        child: Text(
                          'Done',
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: Colors.white
                          ),
                        ),
                      ),
                      onTap: ()  {
                        List<dynamic> selectedList = [];
                        for (var i = 0; i < selectedSpeaker.length; i++) {
                          selectedList.add(finalUserList[selectedSpeaker[i]]);
                        }
                        Navigator.pop(context, [true, selectedList]);
                      },
                    ),
                    SizedBox(height: 20.0,),
                  ],
                ),
              ],
            ),
          ) : Container()
        ],
      ),
    );
  }
}
