import 'package:flutter/material.dart';

class ExitBottomSheet extends StatelessWidget {
  final Function exitButton;

  const ExitBottomSheet({Key key,this.exitButton}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding:
            const EdgeInsets.only(top: 10, right: 20, left: 20, bottom: 20),
        child: Column(
          children: [
            Container(
              width: 40,
              height: 5,
              decoration: BoxDecoration(
                color: Colors.grey,
                borderRadius: BorderRadius.circular(30),
              ),
            ),
            SizedBox(height: 30),
            InkWell(
              onTap: exitButton,
              child: Text(
                "Exit room",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Divider(thickness: 1, height: 60, indent: 20, endIndent: 20),
            InkWell(
              onTap: () => {Navigator.pop(context)},
              child: Text(
                "Cancel",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          ],
        ));
  }
}
