import 'dart:io';
import 'package:clubhouse/screens/followers/twitter_followings.dart';
import 'package:clubhouse/screens/phone/widgets/image_cropper.dart';
import 'package:clubhouse/utils/router.dart';
import 'package:clubhouse/services/authenticate.dart';
import 'package:clubhouse/utils/app_color.dart';
import 'package:clubhouse/widgets/rounded_button.dart';
import 'package:clubhouse/widgets/rounded_image.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:clubhouse/core/data.dart';
import 'package:flutter_twitter/flutter_twitter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart' as Path;

/// The screen for entering a phone number
/// Made up of three main components: title, form, bottom part

class PhoneScreen extends StatefulWidget {
  @override
  _PhoneScreenState createState() => _PhoneScreenState();
}

class _PhoneScreenState extends State<PhoneScreen> {
  final _phoneNumberController = TextEditingController();
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  String verificationId;
  String countryCode = "+380";
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final picker = ImagePicker();
  File imageFile;

  Future<void> checkStoragePermission() async {
    var status = await Permission.storage.status;
    if (status.isUndetermined || status.isDenied) {
      if (await Permission.storage.request().isGranted) {
        getImage();
      }
    }
    if (status.isGranted) {
      getImage();
    }
  }

  Future getImage() async {
    FocusScope.of(context).unfocus();
    new TextEditingController().clear();

    final pickedFile = await picker.getImage(source: ImageSource.gallery, imageQuality: 50);
    imageFile = File(pickedFile.path);

    final result = await Navigator
        .push(context,
        new MaterialPageRoute(
            builder: (context) =>
                ImageCropper(
                    profile: imageFile)));
    if(result[0]) {
      setState(() {
        imageFile = result[1];
      });
      uploadFile();
    }
  }
  String fileUrl = "https://p.kindpng.com/picc/s/78-786207_user-avatar-png-user-avatar-icon-png-transparent.png";
  uploadFile() async {
    var child = 'clubHouseImages/${Path.basename(imageFile.path)}';

    StorageReference storageReference = FirebaseStorage.instance.ref().child(child);
    StorageUploadTask uploadTask = storageReference.putFile(imageFile);

    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    fileUrl = await taskSnapshot.ref.getDownloadURL();

  }
  Future<void> verifyPhone(phoneNumber) async {
    final PhoneVerificationCompleted verified = (AuthCredential authResult) {
      AuthService().signIn(context, authResult);
    };

    final PhoneVerificationFailed verificationfailed =
        (AuthException authException) {
      print('${authException.message}');
    };

    final PhoneCodeSent smsSent = (String verId, [int forceResend]) {
      this.verificationId = verId;

      Navigator.of(context)
          .pushNamed(Routers.sms, arguments: this.verificationId);
    };

    final PhoneCodeAutoRetrievalTimeout autoTimeout = (String verId) {
      this.verificationId = verId;
    };

    await FirebaseAuth.instance.verifyPhoneNumber(

        /// Make sure to prefix with your country code
        phoneNumber: phoneNumber,

        ///No duplicated SMS will be sent out upon re-entry (before timeout).
        timeout: const Duration(seconds: 5),

        /// If the SIM (with phoneNumber) is in the current device this function is called.
        /// This function gives `AuthCredential`. Moreover `login` function can be called from this callback
        /// When this function is called there is no need to enter the OTP, you can click on Login button to sigin directly as the device is now verified
        verificationCompleted: verified,

        /// Called when the verification is failed
        verificationFailed: verificationfailed,

        /// This is called after the OTP is sent. Gives a `verificationId` and `code`
        codeSent: smsSent,

        /// After automatic code retrival `tmeout` this function is called
        codeAutoRetrievalTimeout: autoTimeout);

  }

  void loginTwitter() async {
    var twitterLogin = new TwitterLogin(
      consumerKey: 'A0yBrp0TlM4tigdf1YFB2m3k8',
      consumerSecret: 'YIFvqya8wabGh7dbYYPJqoFB2WUWVlqi006RNrx51cYpHtXBFG',
    );
    final TwitterLoginResult result = await twitterLogin.authorize();
    switch (result.status) {
      case TwitterLoginStatus.loggedIn:
        var session = result.session;
        final AuthCredential credential = TwitterAuthProvider.getCredential(
            authToken: session.token,
            authTokenSecret: session.secret
        );
        FirebaseUser firebaseUser = (await _auth.signInWithCredential(
            credential)).user;
        print('Twitter Login');
        final prefsFirst = await SharedPreferences.getInstance();
        prefsFirst.setString("firstName", result.session.username);

        final prefsLast = await SharedPreferences.getInstance();
        prefsLast.setString("lastName", '');

        final prefsProfile = await SharedPreferences.getInstance();
        prefsProfile.setString("profileImage", firebaseUser.photoUrl);

        final prefsTwitterId = await SharedPreferences.getInstance();
        prefsTwitterId.setString("twitterId", result.session.userId);

        AuthService().addUser(firebaseUser.uid, result.session.userId);
        final PageRouteBuilder _route = new PageRouteBuilder(
          pageBuilder: (BuildContext context, _, __) {
            return TwitterFollowing();
          },
        );
        Navigator.pushAndRemoveUntil(context, _route, (Route<dynamic> r) => false);

        //EasyLoading.show(status: 'Loading',dismissOnTap: false,maskType: EasyLoadingMaskType.black);
        //_apiPresenter.doSocialLogin(firebaseUser.displayName,"",firebaseUser.email, firebaseUser.uid, 'twitter', context);
        break;
      case TwitterLoginStatus.cancelledByUser:
        print("Cancel");
        break;
      case TwitterLoginStatus.error:
        print(result.errorMessage);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(),
      body: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Column(
          children: [
            title(),
            SizedBox(height: 30),
            form(),
            Spacer(),
            bottom(),
          ],
        ),
      ),
    );
  }

  Widget title() {
    return Text(
      'Enter your details #',
      style: TextStyle(fontSize: 25),
    );
  }

  Widget form() {
    return Column(
      children: [
        InkWell(
          child: Container(
            height: 60,
            width: 60,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              image: new DecorationImage(
                image: imageFile != null ? FileImage(imageFile) : AssetImage(''),
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.circular(30),
            ),
            child: imageFile == null ? RoundedImage(
              width: 60,
              height: 60,
              borderRadius: 30,
              path: 'assets/images/profile.png',
            ) : Container(),
          ),
          onTap: () {
            checkStoragePermission();
          },
        ),
        SizedBox(height: 16),
        Container(
          width: 330,
          decoration: BoxDecoration(
            color: AppColor.primaryColor,
            borderRadius: BorderRadius.circular(8),
          ),
          child: Row(
            children: [
              CountryCodePicker(
                onChanged: (code) {
                  setState(() {
                    countryCode = code.dialCode;
                  });
                },
                dialogBackgroundColor: AppColor.primaryColor,
                backgroundColor: AppColor.primaryColor,
                initialSelection: 'UA',
                showCountryOnly: false,
                alignLeft: false,
                padding: const EdgeInsets.all(8),
                textStyle: TextStyle(fontSize: 20),
              ),
              Expanded(
                child: Form(
                  child: TextFormField(
                    controller: _phoneNumberController,
                    autocorrect: false,
                    autofocus: false,
                    decoration: InputDecoration(
                      hintText: 'Phone Number',
                      hintStyle: TextStyle(
                        fontSize: 20,
                      ),
                      border: InputBorder.none,
                    ),
                    keyboardType: TextInputType.numberWithOptions(
                        signed: true, decimal: true),
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.black87,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 16),
        Container(
          width: 330,
          decoration: BoxDecoration(
            color: AppColor.primaryColor,
            borderRadius: BorderRadius.circular(8),
          ),
          child: Padding(
            padding: const EdgeInsets.only(left:8.0),
            child: Form(
              child: TextFormField(
                controller: _firstNameController,
                autocorrect: false,
                autofocus: false,
                decoration: InputDecoration(
                  hintText: 'First Name',
                  hintStyle: TextStyle(
                    fontSize: 20,
                  ),
                  border: InputBorder.none,
                ),
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.black87,
                    fontWeight: FontWeight.w400),
              ),
            ),
          ),
        ),
        SizedBox(height: 16),
        Container(
          width: 330,
          decoration: BoxDecoration(
            color: AppColor.primaryColor,
            borderRadius: BorderRadius.circular(8),
          ),
          child: Padding(
            padding: const EdgeInsets.only(left:8.0),
            child: Form(
              child: TextFormField(
                controller: _lastNameController,
                autocorrect: false,
                autofocus: false,
                decoration: InputDecoration(
                  hintText: 'Last Name',
                  hintStyle: TextStyle(
                    fontSize: 20,
                  ),
                  border: InputBorder.none,
                ),
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.black87,
                    fontWeight: FontWeight.w400),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget bottom() {
    return Column(
      children: [
        Text(
          'OR',
          style: TextStyle(color: Colors.black54),
        ),
        SizedBox(height: 15,),
        Text(
          'Login with',
          style: TextStyle(color: Colors.black87),
        ),
        IconButton(icon: RoundedImage(path: 'assets/images/twitter.png'), onPressed: loginTwitter),
        SizedBox(height: 16),
        Text(
          'By entering your number, you\'re agreeing to out\nTerms or Services and Privacy Policy. Thanks!',
          style: TextStyle(color: Colors.grey),
        ),
        SizedBox(height: 30),
        RoundedButton(
          color: AppColor.AccentBlue,
          minimumWidth: 230,
          disabledColor: AppColor.AccentBlue.withOpacity(0.3),
          onPressed: () async {
            print(countryCode);
            if(_firstNameController.text.length != 0 && _lastNameController.text.length != 0) {
              verifyPhone(countryCode+_phoneNumberController.text);
              profileData = {
                'name': _firstNameController.text,
                'username': _lastNameController.text,
                'profilePicture': fileUrl,
              };
              final prefsFirst = await SharedPreferences.getInstance();
              prefsFirst.setString("firstName", _firstNameController.text);
              prefsFirst.setString("lastName", _lastNameController.text);
              prefsFirst.setString("profileImage", fileUrl);
            }
          },
          child: Container(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Next',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                Icon(Icons.arrow_right_alt, color: Colors.white),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
