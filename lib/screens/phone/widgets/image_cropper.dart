import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:image_crop/image_crop.dart';

class ImageCropper extends StatefulWidget {
  File profile;

  ImageCropper({Key key, @required this.profile}) : super(key: key);

  @override
  _ImageCropperState createState() {
    profile = this.profile;
    return _ImageCropperState();
  }
}

class _ImageCropperState extends State<ImageCropper> {
  final cropKey = GlobalKey<CropState>();
  File _file;

  @override
  void dispose() {
    super.dispose();
    _file?.delete();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(),
      body: Container(
        child: _buildCroppingImage(),
      ),
    );
  }

  Widget _buildCroppingImage() {
    return Padding(
      padding: const EdgeInsets.only(top: 30.0, left: 16.0, right: 16.0, bottom: 50.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Crop.file(widget.profile, key: cropKey),
          ),
          Container(
            padding: const EdgeInsets.only(top: 20.0),
            alignment: AlignmentDirectional.center,
            child: TextButton(
              child: Text(
                'Crop Image',
                style: Theme.of(context)
                    .textTheme
                    .button
                    .copyWith(color: Colors.black54),
              ),
              onPressed: () => _cropImage(),
            ),
          )
        ],
      ),
    );
  }

  Future<void> _cropImage() async {
    final scale = cropKey.currentState.scale;
    final area = cropKey.currentState.area;
    if (area == null) {
      // cannot crop, widget is not setup
      return;
    }

    // scale up to use maximum possible number of pixels
    // this will sample image in higher resolution to make cropped image larger
    final sample = await ImageCrop.sampleImage(
      file: widget.profile,
      preferredSize: (2000 / scale).round(),
    );

    final file = await ImageCrop.cropImage(
      file: sample,
      area: area,
    );

    sample.delete();
    Navigator.pop(context, [true, file]);
  }
}