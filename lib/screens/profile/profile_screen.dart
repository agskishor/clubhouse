import 'package:clubhouse/utils/app_color.dart';
import 'package:clubhouse/utils/router.dart';
import 'package:clubhouse/models/models.dart';
import 'package:clubhouse/services/authenticate.dart';
import 'package:clubhouse/widgets/rounded_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Contain information about current user profile

class ProfileScreen extends StatefulWidget {
  User profile;

  ProfileScreen(
      {Key key, @required this.profile})
      : super(key: key);

  @override
  _ProfileScreenState createState() {
    profile = this.profile;
    return _ProfileScreenState();
  }
}



class _ProfileScreenState extends State<ProfileScreen> {
  int followings = 0;
  int followers = 0;
  void getUserDetails() async {

    final prefs = await SharedPreferences.getInstance();
    String userId = prefs.getString("id");
    final collection = Firestore.instance.collection('users').where('id',isEqualTo: userId);
    var tokens = await collection.getDocuments();
    print('followings ' + tokens.documents.length.toString() + ' ' + userId);
    tokens.documents.forEach((element) {
      List<dynamic> foll = element.data['followers'];
      if(foll != null) {
        setState(() {
          followings = foll.length;
        });
      }
      print('followings ' + followings.toString());
    });
    final collectionFollowers = Firestore.instance.collection('users').where('followers',arrayContains: userId);
    var followersDoc = await collectionFollowers.getDocuments();
    if(followersDoc != null) {
      setState(() {
        followers = followersDoc.documents.length;
        print('followers ' + followers.toString());
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserDetails();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.primaryColor,
        actions: [
          // Button that logout user
          IconButton(
            icon: Icon(Icons.logout),
            onPressed: () {
              AuthService().signOut();
              Navigator.of(context)
                  .pushNamedAndRemoveUntil(Routers.phone, (route) => false);
            },
          ),
        ],
        centerTitle: true,
        title: Text(
          'Profile',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Stack(
        children: [
          header(),
          profileBody(),
          Container(
            height: 170,
            alignment: Alignment.center,
            child: Column(
              children: [
                Container(height: 70 - (MediaQuery.of(context).padding.top / 2),),
                widget.profile.profilePicture.length == 2 ? Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        color: AppColor.BackgroundColor,
                        borderRadius: BorderRadius.circular(10)
                    ),
                    child: Center(child: Text(widget.profile.profilePicture,
                      style: TextStyle(fontWeight: FontWeight.bold,color: AppColor.BackgroundColor, fontSize: 20),))
                ): RoundedImage(
                  width: 100,
                  height: 100,
                  borderRadius: 50,
                  url: widget.profile.profilePicture ?? "",
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget header() {
    return Container(
      height: 120,
      color: AppColor.primaryColor,
    );
  }

  Widget profileBody() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: Get.height - 170 - MediaQuery
            .of(context)
            .padding
            .top,
        width: Get.width,
        decoration: BoxDecoration(
          color: AppColor.BackgroundColor,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(18.0),
            topRight: Radius.circular(18.0),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(height: 75,),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        widget.profile.name,
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 20),
                      Container(
                        height: 75,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black12, width: 1),
                            borderRadius: BorderRadius.circular(10.0)
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Row(
                            children: [
                              Container(
                                width: Get.width / 2 - 22,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(5)),
                                    ),
                                height: 60,
                                alignment: Alignment.center,
                                child: Column(
                                  children: [
                                    SizedBox(height: 4.0,),
                                    Text(
                                      'Followers',
                                      style: TextStyle(
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.w600,
                                          color:  Colors.black38
                                      ),
                                    ),
                                    SizedBox(height: 8.0,),
                                    Text(
                                      followers.toString(),
                                      style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    SizedBox(height: 4.0,),
                                  ],
                                ),
                              ),
                              Container(
                                height: 40,
                                width: 1,
                                color: Colors.black12,
                              ),
                              Container(
                                width: Get.width / 2 - 22,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(5)),
                                ),
                                height: 60,
                                alignment: Alignment.center,
                                child: Column(
                                  children: [
                                    SizedBox(height: 4.0,),
                                    Text(
                                      'Followings',
                                      style: TextStyle(
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.w600,
                                        color:  Colors.black38
                                      ),
                                    ),
                                    SizedBox(height: 8.0,),
                                    Text(
                                      followings.toString(),
                                      style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    SizedBox(height: 4.0,),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'About',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: 5.0,),
                          Text(
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                                ' Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. '
                                'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. '
                                'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum Lorem'
                                ' ipsum dolor sit amet, consectetur adipiscing elit'
                            ,
                            style: TextStyle(fontSize: 13, fontWeight: FontWeight.normal,color: Colors.black38),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
