import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:clubhouse/models/models.dart';
import 'package:clubhouse/screens/home/widgets/home_bottom_sheet.dart';
import 'package:clubhouse/screens/home/widgets/room_card.dart';
import 'package:clubhouse/screens/room/room_screen.dart';
import 'package:clubhouse/utils/app_color.dart';
import 'package:clubhouse/widgets/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:clubhouse/screens/followers/add_follower.dart';
import 'package:clubhouse/widgets/rounded_image.dart';
import 'package:clubhouse/utils/router.dart';
import 'package:clubhouse/models/models.dart';
import 'package:clubhouse/core/data.dart';

/// Fetch Rooms list from `Firestore`
/// Use `pull_to_refresh` plugin, which provides pull-up load and pull-down refresh for room list

class RoomsList extends StatefulWidget {
  final String id;
  final User profile;

  const RoomsList({Key key, this.id, this.profile}) : super(key: key);

  @override
  _RoomsListState createState() => _RoomsListState();
}

class _RoomsListState extends State<RoomsList> {
  // Setting reference to 'rooms' collection
  var collection = Firestore.instance.collection('rooms');
  final collectionToken = Firestore.instance.collection('token');
  String token = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();
  }

  void getToken() async {
    var tokens = await collectionToken.getDocuments();
    tokens.documents.forEach((element) {
      setState(() {
        token = element.data['token'];
        print("token " + token);
      });
      agoraToken = element.data['token'];
    });
  }

  Widget roomCard(Room room, String documentID) {
    return GestureDetector(
      onTap: () async {
        // Launch user microphone permission
        await Permission.microphone.request();

        final prefs = await SharedPreferences.getInstance();
        int userRole = 0;
        room.users.forEach((element) {
          if (element.id == prefs.getString("id")) {
            element.status = 1; //for join
            userRole = element.role;
          }
        });
        collection.document(documentID).updateData(room.toJson());
        // Open BottomSheet dialog

        showModalBottomSheet(
          isScrollControlled: true,
          context: context,
          builder: (context) {
            return RoomScreen(
              room: room,
              id: documentID,
              role: userRole,
            );
          },
        );
        /*Navigator.of(context)
            .pushNamed(Routers.roomScreen);*/
      },
      child: Container(
        margin: const EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 15,
        ),
        child: RoomCard(room: room),
      ),
    );
  }

  /// Adds a smoothed blur at the bottom of the screen when scroll the list

  Widget gradientContainer() {
    return Container(
      height: 50,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            AppColor.BackgroundColor.withOpacity(0.2),
            AppColor.BackgroundColor,
          ],
        ),
      ),
    );
  }

  Widget startRoomButton() {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(width: 10),
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AddFollower()),
              );
            },
            child: Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                    color: AppColor.AccentGreen,
                    borderRadius: BorderRadius.circular(20)),
                child:
                    Icon(Icons.group_add_sharp, size: 25, color: Colors.white)),
          ),
          SizedBox(width: 10),
          Container(
            child: RoundedButton(
                onPressed: () {
                  showModalBottomSheet(
                    context: context,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15),
                      ),
                    ),
                    builder: (context) {
                      return Wrap(
                        children: [
                          HomeBottomSheet(),
                        ],
                      );
                    },
                  );
                },
                color: AppColor.AccentGreen,
                text: '+ Start a room'),
          ),
          SizedBox(width: 10),
          GestureDetector(
            onTap: () => {
              Navigator.of(context)
                  .pushNamed(Routers.profile, arguments: widget.profile)
            },
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                  color: AppColor.LightBrown,
                  borderRadius: BorderRadius.circular(20)),
              child: widget.profile.profilePicture.length == 2
                  ? Center(
                      child: Text(
                      widget.profile.profilePicture,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: AppColor.BackgroundColor,
                          fontSize: 18),
                    ))
                  : RoundedImage(
                      width: 50,
                      height: 50,
                      borderRadius: 25,
                      url: widget.profile.profilePicture ?? "",
                    ),
            ),
          ),
          SizedBox(width: 10)
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        // Making a StreamBuilder to listen to changes in real time
        StreamBuilder<QuerySnapshot>(
          stream: collection
              .where("users_ids", arrayContains: widget.id)
              .where("status", isEqualTo: 1)
              .snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            // Handling errors from firebase

            if (snapshot.hasError) return Text('Error: ${snapshot.error}');

            return snapshot.hasData
                ? ListView(
                    children: snapshot.data.documents
                        .map((DocumentSnapshot document) {
                      //print("update room : " + Room.fromJson(document).users[0].name);
                      return roomCard(
                          Room.fromJson(document), document.documentID);
                    }).toList(),
                  )
                // Display if still loading data
                : Center(
                    child: CircularProgressIndicator(),
                  );
          },
        ),
        gradientContainer(),
        startRoomButton(),
      ],
    );
  }
}
