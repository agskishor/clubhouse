import 'package:clubhouse/models/models.dart';
import 'package:clubhouse/utils/app_color.dart';
import 'package:clubhouse/widgets/rounded_image.dart';
import 'package:flutter/material.dart';

/// Each element that fetch from Firestore return the RoomCard

class RoomCard extends StatelessWidget {
  final Room room;

  const RoomCard({Key key, this.room}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      decoration: BoxDecoration(
          color: AppColor.primaryColor,
          borderRadius: BorderRadius.circular(20),
          ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            room.title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18,),
          ),
          SizedBox(height: 10),
          Text(
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
            style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12,color: Colors.black45),
          ),
          SizedBox(height : 10),
          Row(
            children: [
              RoundedImage(path: 'assets/images/user.png',url: room.users[0].profilePicture,),
              SizedBox(width : 5),
              RoundedImage(path: 'assets/images/user.png',url: room.users[1].profilePicture,width: 35,height: 35,),
              SizedBox(width: 10),
              Expanded(
                child: Container(),
              ),
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 6,
                  vertical: 6,
                ),
                decoration: BoxDecoration(
                  color: AppColor.BackgroundColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  children: [
                    Icon(Icons.supervisor_account_sharp, size: 18, color: Colors.black54),
                    SizedBox(width: 8,),
                    Text(
                      room.users.length.toString(),
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14,),
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget profileImages() {
    return Stack(
      children: [
        RoundedImage(
          margin: const EdgeInsets.only(top: 15, left: 25),
          path: 'assets/images/profile.png',
        ),
        RoundedImage(path: 'assets/images/profile.png'),
      ],
    );
  }

  Widget usersList() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        for (var i = 0; i < room.users.length; i++)
          Container(
            child: Row(
              children: [
                Text(
                  room.users[i].name,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(width: 5),
                Icon(Icons.chat, color: Colors.grey, size: 14),
              ],
            ),
          )
      ],
    );
  }

  Widget roomInfo() {
    return Row(
      children: [
        Text(
          '${room.users.length}',
          style: TextStyle(color: Colors.grey),
        ),
        Icon(Icons.supervisor_account, color: Colors.grey, size: 14),
        Text(
          '  /  ',
          style: TextStyle(color: Colors.grey, fontSize: 10),
        ),
        Text(
          '${room.speakerCount}',
          style: TextStyle(color: Colors.grey),
        ),
        Icon(Icons.chat_bubble_rounded, color: Colors.grey, size: 14),
      ],
    );
  }
}
