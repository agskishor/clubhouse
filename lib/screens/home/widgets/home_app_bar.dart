import 'package:clubhouse/models/models.dart';
import 'package:clubhouse/screens/followers/add_follower.dart';
import 'package:clubhouse/utils/app_color.dart';
import 'package:clubhouse/widgets/rounded_image.dart';
import 'package:flutter/material.dart';

class HomeAppBar extends StatelessWidget {
  final User profile;
  final Function onProfileTab;

  const HomeAppBar({Key key, this.profile, this.onProfileTab})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [

        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddFollower()),
            );
          },
          child: Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
                color: AppColor.LightBrown,
                borderRadius: BorderRadius.circular(20)
            ),
            child: Icon(Icons.group_add_sharp, size: 30, color: Colors.black)
          ),
        ),
        GestureDetector(
          onTap: onProfileTab,
          child: Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
                color: AppColor.LightBrown,
                borderRadius: BorderRadius.circular(20)
            ),
            child: profile.profilePicture.length == 2 ?  Center(child: Text(profile.profilePicture,style:
            TextStyle(fontWeight: FontWeight.bold, color: AppColor.BackgroundColor ,fontSize: 18),)) :
             RoundedImage(
              width: 50,
              height: 50,
              borderRadius: 25,
              url: profile.profilePicture ?? "",
            ),
          ),
          // RoundedImage(
          //   path: profile.profileImage,
          //   width: 40,
          //   height: 40,
          // ),
        )
      ],
    );
  }
}
