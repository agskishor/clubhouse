import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:clubhouse/core/data.dart';
import 'package:clubhouse/screens/room/speaker_screen.dart';
import 'package:clubhouse/utils/app_color.dart';
import 'package:clubhouse/widgets/rounded_button.dart';
import 'package:clubhouse/widgets/rounded_image.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:clubhouse/screens/room/room_screen.dart';
import 'package:clubhouse/models/models.dart';
import 'package:agora_rtc_engine/rtc_engine.dart';

/// Open when the user wants to create a new room.

class HomeBottomSheet extends StatefulWidget {
  const HomeBottomSheet({Key key}) : super(key: key);

  @override
  _HomeBottomSheetState createState() => _HomeBottomSheetState();
}

class _HomeBottomSheetState extends State<HomeBottomSheet> {
  var selectedButtonIndex = 0;
  final _topicController = TextEditingController();
  bool _validate = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 10, right: 20, left: 20, bottom: 20),
      child: Column(
        children: [
          Container(
            width: 40,
            height: 5,
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(30),
            ),
          ),
          SizedBox(height: 30),
          TextField(
            controller: _topicController,
            autocorrect: false,
            autofocus: false,
            decoration: InputDecoration(
              hintText: 'Enter Room Topic',
              errorText: _validate ? 'Please enter room topic' : null,
              hintStyle: TextStyle(
                fontSize: 16,
              ),
            ),
            style: TextStyle(
                fontSize: 20, color: Colors.black87, fontWeight: FontWeight.w400),
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              // List of 3 rooms: Open, Social, and Closed
              for (var i = 0, len = 3; i < len; i++) roomCard(i),
            ],
          ),
          Divider(thickness: 1, height: 60, indent: 20, endIndent: 20),
          Text(
            bottomSheetData[selectedButtonIndex]['selectedMessage'],
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 20),
          letsGoButton()
        ],
      ),
    );
  }

  Widget roomCard(int i) {
    return InkWell(
      borderRadius: BorderRadius.circular(15),
      onTap: () {
        setState(() {
          selectedButtonIndex = i;
        });
        /*if(i == 1){
          openSpeakerList("hsdhfghdsbf");
        }*/
        if (i == 2) {
          openMemberList();
        }
      },
      child: Ink(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        decoration: BoxDecoration(
          color: i == selectedButtonIndex
              ? AppColor.SelectedItemGrey
              : Colors.transparent,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(
            color: i == selectedButtonIndex
                ? AppColor.SelectedItemBorderGrey
                : Colors.transparent,
          ),
        ),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(bottom: 5),
              child: RoundedImage(
                width: 50,
                height: 50,
                borderRadius: 20,
                path: bottomSheetData[i]['image'],
              ),
            ),
            Text(
              bottomSheetData[i]['text'],
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }

  Widget letsGoButton() {
    return RoundedButton(
        color: AppColor.AccentGreen,
        onPressed: createRoom,
        text: '🎉 Let\'s go');
  }

  void createRoom() async {
    if (_topicController.text.length > 0) {
      final collection = Firestore.instance.collection('rooms');
      final prefs = await SharedPreferences.getInstance();
      profileData = {
        'name':
            prefs.getString("firstName") + ' ' + prefs.getString("lastName"),
        'id': prefs.getString("id"),
        'profilePicture': prefs.getString("profileImage"),
        'role': 3, // for modarator
        'status': 1 //1 => joined, 0 => not joined
      };
      print("call create room open");
      int time = DateTime.now().millisecondsSinceEpoch;
      String id = "";
      await collection.add(
        {
          'title': _topicController.text,
          'users': [profileData],
          'speakerCount': 1,
          'status': 1, //1 for live and 0 for closed
          'type': selectedButtonIndex + 1, // 1 => Open, 2 => Social, 3 => Closed
          'timestamp' : time
        },
      ).then((value) => id = value.documentID);
      var room = Room();
      if (selectedButtonIndex == 0)
        room = await addFollowersInRoomForOpen(id,time);
      else if (selectedButtonIndex == 1) {
        room = await openSpeakerList(id,time);
      } else if (selectedButtonIndex == 2) {
        if (closedUser.length > 0) {
          room = await openSpeakerList(id,time);
        }
      }
      if (room != null) {
        // Launch user microphone permission
        await Permission.microphone.request();
        Navigator.pop(context);
        // Open BottomSheet dialog
        showModalBottomSheet(
          isScrollControlled: true,
          context: context,
          builder: (context) {
            return RoomScreen(
                room: room,
                // Pass user role
                role: 3,
                id: id);
          },
        );
      }
    } else {
      setState(() {
        _topicController.text.isEmpty ? _validate = true : _validate = false;
      });
    }
  }

  Future<Room> openSpeakerList(String documentId,int time) async {
    final List<dynamic> result = await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => SpeakerScreen(isTypeSocial: true)));
    if (result[0]) {
      var speakerData = result[1];
      var ownData = {
        'name': speakerData['name'],
        'id': speakerData['fireStoreId'],
        'profilePicture': speakerData['profile_image_url'],
        'role': 2, // for speaker
        'status': 0, //1 => joined, 0 => not joined
        'speakRequest': 2
      };
      print('speaker data ' + ownData.toString());
      if (selectedButtonIndex == 1) {
        var room = await addFollowerInRoomForSocial(documentId, ownData,time);
        return room;
      } else {
        var room = await addFollowerInRoomForClosed(documentId, ownData,time);
        return room;
      }
    }
    return null;
  }

  Future<Room> addFollowerInRoomForSocial(
      String documentId, dynamic speakerData,int time) async {
    final prefs = await SharedPreferences.getInstance();
    List<dynamic> users = [];
    List<String> usersArray = [];
    //add own data in users
    var ownData = {
      'name': prefs.getString("firstName") + ' ' + prefs.getString("lastName"),
      'id': prefs.getString("id"),
      'profilePicture': prefs.getString("profileImage"),
      'role': 3, // for modarator
      'status': 1, //1 => joined, 0 => not joined
      'speakRequest': 2
    };
    users.add(ownData);
    usersArray.add(prefs.getString("id"));

    //add speaker data
    users.add(speakerData);
    usersArray.add(speakerData['id']);
    var followerDocuments = await Firestore.instance
        .collection('users')
        .where("followers", arrayContains: prefs.getString("id"))
        .getDocuments();
    followerDocuments.documents.forEach((element) {
      print("follower " + element.data['id']);
      if (element.data['id'] != speakerData['id']) {
        var userProfile = {
          'name': element.data['name'],
          'id': element.data['id'],
          'profilePicture': element.data['profilePicture'],
          'role': 1, // for audiance
          'status': 0, //1 => joined, 0 => not joined
          'speakRequest': 0
        };
        users.add(userProfile);
        usersArray.add(element.data['id']);
      }
    });
    final collection = Firestore.instance.collection('rooms');
    var roomJson = {
      'title': _topicController.text,
      'users': users,
      'speakerCount': 1,
      'status': 1,
      'id': documentId,
      'type': selectedButtonIndex + 1, // 1 => Open, 2 => Social, 3 => Closed
      'users_ids': usersArray,
      'timestamp' : time
    };
    collection.document(documentId).setData(roomJson);
    var room = Room.fromJson(roomJson);
    return room;
  }

  Future<Room> addFollowersInRoomForOpen(String documentId,int time) async {
    final prefs = await SharedPreferences.getInstance();
    List<dynamic> users = [];
    List<String> usersArray = [];
    String userId = prefs.getString("id");
    //add own data in users
    var ownData = {
      'name': prefs.getString("firstName") + ' ' + prefs.getString("lastName"),
      'id': userId,
      'profilePicture': prefs.getString("profileImage"),
      'role': 3, // for modarator
      'status': 1, //1 => joined, 0 => not joined
      'speakRequest': 2
    };
    users.add(ownData);
    usersArray.add(userId);
    var allUsers = await Firestore.instance.collection('users').getDocuments();
    allUsers.documents.forEach((element) {
      print("follower " + element.data['id']);
      if (element.data['id'] != userId) {
        var userProfile = {
          'name': element.data['name'],
          'id': element.data['id'],
          'profilePicture': element.data['profilePicture'],
          'role': 2, // for speaker
          'status': 0, //1 => joined, 0 => not joined
          'speakRequest': 0
        };
        users.add(userProfile);
        usersArray.add(element.data['id']);
      }
    });
    final collection = Firestore.instance.collection('rooms');
    var roomJson = {
      'title': _topicController.text,
      'users': users,
      'speakerCount': 1,
      'status': 1,
      'id': documentId,
      'type': selectedButtonIndex + 1, // 1 => Open, 2 => Social, 3 => Closed
      'users_ids': usersArray,
      'timestamp' : time
    };
    collection.document(documentId).setData(roomJson);
    var room = Room.fromJson(roomJson);
    return room;
  }

  List<dynamic> closedUser = [];

  void openMemberList() async {
    final List<dynamic> result = await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => SpeakerScreen(isTypeSocial: false)));
    closedUser = result[1];

  }

  Future<Room> addFollowerInRoomForClosed(
      String documentId, dynamic speakerData,int time) async {
    final prefs = await SharedPreferences.getInstance();
    List<dynamic> users = [];
    List<String> usersArray = [];

    //add own data in users
    var ownData = {
      'name': prefs.getString("firstName") + ' ' + prefs.getString("lastName"),
      'id': prefs.getString("id"),
      'profilePicture': prefs.getString("profileImage"),
      'role': 3, // for modarator
      'status': 1, //1 => joined, 0 => not joined
      'speakRequest': 2
    };
    users.add(ownData);
    usersArray.add(prefs.getString("id"));

    //add speaker data
    users.add(speakerData);
    usersArray.add(speakerData['id']);

    if (closedUser.length > 0) {
      closedUser.forEach((element) {
        if (element['fireStoreId'] != speakerData['id']) {
          var member = {
            'name': element['name'],
            'id': element['fireStoreId'],
            'profilePicture': element['profile_image_url'],
            'role': 1, // for audiance
            'status': 0, //1 => joined, 0 => not joined
            'speakRequest': 2
          };
          users.add(member);
          usersArray.add(element['fireStoreId']);
        }
      });
    }
    final collection = Firestore.instance.collection('rooms');
    var roomJson = {
      'title': _topicController.text,
      'users': users,
      'speakerCount': 1,
      'status': 1,
      'id': documentId,
      'type': selectedButtonIndex + 1, // 1 => Open, 2 => Social, 3 => Closed
      'users_ids': usersArray,
      'timestamp' : time
    };
    collection.document(documentId).setData(roomJson);
    var room = Room.fromJson(roomJson);
    return room;
  }
}
