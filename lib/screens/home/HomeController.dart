import 'package:clubhouse/models/models.dart';
import 'package:get/get.dart';

class HomeController extends GetxController{
  var roomDetail = Room().obs;

  void setRoom(Room room){
    roomDetail.value = room;
  }

}